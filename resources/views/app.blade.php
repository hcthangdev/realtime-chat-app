<!DOCTYPE html>
<html lang="en">
@include('common.head')
<body>
    <div id="app"></div>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
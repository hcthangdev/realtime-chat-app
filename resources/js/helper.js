export class Helper {
    static escapeHtml(text) {
        if (!text)
            return '';
        return text
            .replace(/&amp;/g, '&')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
    }
    static formatCurrency(money) {
        if (!money)
            return '0';
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    static formatPhone(phone) {
        if (!phone)
            return '';
        if (phone.startsWith('0') || phone.startsWith('84')) {
            return phone;
        }
        return '0' + phone;
    }

    static numberWithCommas(number, delimiter = ',') {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter)
    }

    static compactNumber(value) {
        const suffixes = ['', 'k', 'm', 'b', 't'];
        const suffixNum = Math.floor(('' + value).length / 3)
        let shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(2))

        if (shortValue % 1 != 0) {
            shortValue = shortValue.toFixed(1)
        }
        return shortValue + suffixes[suffixNum]
    }

    static ordinalSuffix(number) {
        let j = number % 10,
            k = number % 100;

        if (j == 1 && k != 11) {
            return number + 'st'
        }
        if (j == 2 && k != 12) {
            return number + 'nd'
        }
        if (j == 3 && k != 13) {
            return number + 'rd'
        }
        return number + 'th'
    }
}
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';

require('./bootstrap');

window.Vue = require('vue');
import i18n from './i18n'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.Event = new class {
    constructor() {
        this.vue = new Vue();
    }

    fire(event, data = null) {
        this.vue.$emit(event, data);
    }

    listen(event, callback) {
        this.vue.$on(event, callback);
    }
};

export const eventBus = new Vue({
    created() {
        window.addEventListener('resize', this.busSize)
    },
    methods: {
        busSize() {
            this.$emit('windowWidth', window.innerWidth)
        }
    }
})

import App from './components/App.vue'
import router from './routes.js'

Vue.config.productionTip = false;

const app = new Vue({
    render: h => h(App),
    router,
    i18n,
    data: {
        origin: '',
        test: ''
    }
}).$mount('#app');
import Vue from 'vue'
import VueRouter from 'vue-router'
import NewsFeed from './components/NewsFeed.vue'
import Friends from './components/Friends.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'newsfeed',
        component: NewsFeed
    },
    {
        path: '/friends',
        name: 'friends',
        component: Friends
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
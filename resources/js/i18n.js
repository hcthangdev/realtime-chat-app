import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './lang/en.json'
import vi from './lang/vi.json'
Vue.use(VueI18n)

const filesLang = require.context('./lang', true, /\.json$/i)
export const listLang = filesLang.keys().map(key => key.split('/').pop().split('.')[0])

let locale = 'vi'
let fallbackLocale = 'vi'
let messages = { vi, en }

const i18n = new VueI18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages: messages
})
export default i18n;